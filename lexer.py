#!/usr/bin/python3

################################################################################
##[ MODULES ]###################################################################
################################################################################

import  sys;

################################################################################
##[ FUNCTIONS ]#################################################################
################################################################################

##############################
def printf(format, *args):
    sys.stdout.write(format % args)

##############################
def error(detail):
    print("ERROR: " + detail);
    exit(1);

##############################
def get_brackets(string):
    max = len(string);
    num = 0;
    i = 0;
    j = 0;
    while (j < max):
        if (string[j] == "("):
            num += 1;
        elif (string[j] == ")"):
            num -= 1;
        if (num == 0):
            return (string[i + 1:j]);
        elif (num < 0):
            error("Problem with parenthesis");
        j += 1;
    error("Problem with parenthesis");

##############################
def lexe(string):
    max = len(string);
    list = [];
    i = 0;
    while (i < max):
        if (string[i].isalpha() == True):
            list.append(string[i]);
            i += 1;
        elif (string[i] == "("):
            brackets = get_brackets(string[i:]);
            i += len(brackets) + 2;
            list.append(lexe(brackets));
        elif (string[i] == "!"):
            list.append("!");
            i += 1;
        elif ("\+|^".find(string[i])):
            list.append(string[i]);
            i += 1;
        else:
            error("Unknown character [" + string[i] + "]");
    return (list);
