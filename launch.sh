#!/bin/bash

clear

i=0;
printf -- "---------------------------\n"
printf -- "---------- BASIC ----------\n"
printf -- "---------------------------\n"
for file in `ls tests | grep "basic"`; do
	printf "[%s]:\n" "tests/$file"
	./expert-system "tests/$file"
	printf "\n"
done

printf -- "----------------------------\n"
printf -- "---------- MEDIUM ----------\n"
printf -- "----------------------------\n"
for file in `ls tests | grep "medium"`; do
	printf "[%s]:\n" "tests/$file"
	./expert-system "tests/$file"
	printf "\n"
done
